package com.je;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.apache.servicecomb.springboot2.starter.EnableServiceComb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableServiceComb
@EnableScheduling
@EnableApolloConfig
public class AppMain {

    private static final Logger logger = LoggerFactory.getLogger(AppMain.class);

    public static void main(String[] args) {
        try {
            new SpringApplicationBuilder().web(WebApplicationType.NONE).sources(AppMain.class).run(args);
            logger.info("============================启动成功============================");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
