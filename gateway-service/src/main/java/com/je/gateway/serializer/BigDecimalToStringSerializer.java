package com.je.gateway.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

public class BigDecimalToStringSerializer extends JsonSerializer<BigDecimal> {
    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        String valueString = value.toPlainString();
        if (value.longValue() > 9007199254740992L || value.longValue() < -9007199254740992L) {
            gen.writeString(valueString);
        } else {
            gen.writeNumber(value);
        }
    }
}


