package com.je.gateway.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class LongToStringSerializer extends JsonSerializer<Long> {
    @Override
    public void serialize(Long value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        String valueString = value.toString();
        if (value > 9007199254740992L || value < -9007199254740992L) {
            gen.writeString(valueString);
        } else {
            gen.writeNumber(value);
        }
    }
}