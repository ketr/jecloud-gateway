package com.je.gateway.router.model.func;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: jecloud-gateway
 * @author: LIULJ
 * @create: 2021/7/4
 * @description:
 */
public class ConfigFuncs implements Serializable {

    private String name;

    private List<String> values = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getValues() {
        return values;
    }

    public void addFunc(String value){
        this.values.add(value);
    }

    public void removeFunc(String value){
        this.values.remove(value);
    }
    
}
