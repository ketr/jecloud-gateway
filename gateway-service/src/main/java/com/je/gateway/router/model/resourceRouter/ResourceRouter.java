package com.je.gateway.router.model.resourceRouter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ResourceRouter implements Serializable {

    private Pattern pattern = Pattern.compile("/(.*)");
    private boolean enable = false;
    private List<ResourceSuffix> resourceSuffixes = new ArrayList<>();

    public ResourceRouter() {
    }

    public ResourceRouter(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public void addResource(ResourceSuffix resourceSuffix){
        this.resourceSuffixes.add(resourceSuffix);
    }

    public void removeResource(ResourceSuffix resourceSuffix){
        this.resourceSuffixes.remove(resourceSuffix);
    }


}
