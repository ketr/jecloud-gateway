package com.je.gateway.router.model.resourceRouter;

import java.io.Serializable;

public class ResourceSuffix implements Serializable {

    private String name;
    private String microServiceName;
    private String versionRule;
    private int prefixSegmentCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMicroServiceName() {
        return microServiceName;
    }

    public void setMicroServiceName(String microServiceName) {
        this.microServiceName = microServiceName;
    }

    public String getVersionRule() {
        return versionRule;
    }

    public void setVersionRule(String versionRule) {
        this.versionRule = versionRule;
    }

    public int getPrefixSegmentCount() {
        return prefixSegmentCount;
    }

    public void setPrefixSegmentCount(int prefixSegmentCount) {
        this.prefixSegmentCount = prefixSegmentCount;
    }
}
