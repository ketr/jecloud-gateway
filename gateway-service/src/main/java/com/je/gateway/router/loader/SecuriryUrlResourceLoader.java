package com.je.gateway.router.loader;

import com.je.gateway.router.model.security.SecurityUrl;
import com.je.gateway.xml.AbstractXmlConfigResourceLoader;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

public class SecuriryUrlResourceLoader extends AbstractXmlConfigResourceLoader<SecurityUrl> {

    private static final Logger logger = LoggerFactory.getLogger(SecuriryUrlResourceLoader.class);

    private static final String DEFAULT_CONFIG_PAHT = "security-url.xml";

    public SecuriryUrlResourceLoader() {
        this(DEFAULT_CONFIG_PAHT);
    }

    /**
     * pattern属性标签
     */
    private static final String ROUTER_PATTERN_ATTRIBUTE_KEY = "pattern";


    public SecuriryUrlResourceLoader(String resourcePath) {
        super(resourcePath);
    }

    @Override
    public SecurityUrl parse(Element rootElement) {
        SecurityUrl securityUrl = new SecurityUrl();
        String pattern = rootElement.attributeValue(ROUTER_PATTERN_ATTRIBUTE_KEY);
        securityUrl.setPath(pattern);
        List<Element> elements = rootElement.elements();
        for (Element eachUrlElement : elements) {
            securityUrl.addUrl(eachUrlElement.getStringValue());
        }
        return securityUrl;
    }

}
