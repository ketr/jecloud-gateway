package com.je.gateway.router.loader;

import com.google.common.base.Strings;
import com.je.gateway.router.model.resourceRouter.ResourceRouter;
import com.je.gateway.router.model.resourceRouter.ResourceSuffix;
import com.je.gateway.xml.AbstractXmlConfigResourceLoader;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ResourceRouterResourceLoader extends AbstractXmlConfigResourceLoader<ResourceRouter> {

    private static final Logger logger = LoggerFactory.getLogger(ResourceRouterResourceLoader.class);
    private static final String DEFAULT_CONFIG_PAHT = "resourceRouter.xml";

    /**
     * pattern属性标签
     */
    private static final String ROUTER_PATTERN_ATTRIBUTE_KEY = "pattern";
    /**
     * enable属性标签
     */
    private static final String ROUTER_ENABLE_ATTRIBUTE_KEY = "enable";

    /**
     * suffix name属性
     */
    private static final String ROUTER_PATTERN_SUFFIX_NAME_KEY = "name";
    /**
     * suffix microServiceName属性
     */
    private static final String ROUTER_PATTERN_SUFFIX_MICROSERVICENAME_KEY = "microServiceName";
    /**
     * suffix versionRule属性
     */
    private static final String ROUTER_PATTERN_SUFFIX_VERSIONRULE_KEY = "versionRule";
    /**
     * suffix prefixSegmentCount属性
     */
    private static final String ROUTER_PATTERN_SUFFIX_PREFIXSEGEMENTCOUNT_KEY = "prefixSegmentCount";

    public ResourceRouterResourceLoader() {
        this(DEFAULT_CONFIG_PAHT);
    }

    public ResourceRouterResourceLoader(String resourcePath) {
        super(resourcePath);
    }

    @Override
    public ResourceRouter parse(Element rootElement) {
        //获取pattern配置
        String pattern = rootElement.attributeValue(ROUTER_PATTERN_ATTRIBUTE_KEY);
        String enable = rootElement.attributeValue(ROUTER_ENABLE_ATTRIBUTE_KEY);

        ResourceRouter router;
        if(Strings.isNullOrEmpty(pattern)){
            router = new ResourceRouter();
        }else{
            router = new ResourceRouter(pattern);
        }

        if(!Strings.isNullOrEmpty(enable)){
            if("true".equals(enable)){
                router.setEnable(true);
            }else{
                router.setEnable(false);
            }
        }

        List<Element> elements = rootElement.elements();
        if(elements == null || elements.isEmpty()){
            return router;
        }

        for (Element eachElement: elements) {
            router.addResource(parseResourceSuffix(eachElement));
        }
        return router;
    }

    private ResourceSuffix parseResourceSuffix(Element suffixElement){
        ResourceSuffix resourceSuffix = new ResourceSuffix();
        String name = suffixElement.attributeValue(ROUTER_PATTERN_SUFFIX_NAME_KEY);
        String microServiceName = suffixElement.attributeValue(ROUTER_PATTERN_SUFFIX_MICROSERVICENAME_KEY);
        String versionRule = suffixElement.attributeValue(ROUTER_PATTERN_SUFFIX_VERSIONRULE_KEY);
        String prefixSegementCount = suffixElement.attributeValue(ROUTER_PATTERN_SUFFIX_PREFIXSEGEMENTCOUNT_KEY);
        if(Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(microServiceName)
                || Strings.isNullOrEmpty(versionRule) || Strings.isNullOrEmpty(prefixSegementCount)){
            logger.error("Parse xml configuration resourceRouter.xml error!");
            System.exit(0);
        }
        resourceSuffix.setName(name);
        resourceSuffix.setMicroServiceName(microServiceName);
        resourceSuffix.setVersionRule(versionRule);
        resourceSuffix.setPrefixSegmentCount(Integer.valueOf(prefixSegementCount));
        return resourceSuffix;
    }
}
