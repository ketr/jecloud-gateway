package com.je.gateway.router.dispatcher;

import com.google.common.base.Strings;
import com.je.gateway.router.GlobalRouterParams;
import com.netflix.config.DynamicPropertyFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.PlatformHandler;
import org.apache.servicecomb.edge.core.EdgeInvocation;
import org.apache.servicecomb.transport.rest.vertx.RestBodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 请求头路由器--产品
 */
public class UrlHeaderProductEdgeDispatcher extends GatewayAbstractRestDispatcher {

    private static final Logger logger = LoggerFactory.getLogger(UrlHeaderProductEdgeDispatcher.class);

    private static final String KEY_ENABLED = "servicecomb.http.dispatcher.edge.headerRouter.product.enabled";

    public static final String CONFIGURATION_ITEM = "HeaderRoutedProductConfigurationItem";

    public UrlHeaderProductEdgeDispatcher() {

    }

    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public void init(Router router) {
        String pattern = GlobalRouterParams.GLOBAL_HEADER_ROUTER.getPath();
        router.routeWithRegex(pattern)
                .failureHandler(this::onFailure)
                .handler((PlatformHandler)UrlHeaderProductEdgeDispatcher.this::preCheck)
                .handler(createBodyHandler())
                .handler(this::onRequest);
    }

    protected void preCheck(RoutingContext context) {
        String microServiceName = context.request().getHeader("pd");
        String url =context.request().path();
        if(Strings.isNullOrEmpty(microServiceName) || Strings.isNullOrEmpty(url) ){
            context.put(RestBodyHandler.BYPASS_BODY_HANDLER, Boolean.TRUE);
            context.next();
            return;
        }
        context.next();
    }


    protected void onRequest(RoutingContext context) {
        String microServiceName = context.request().getHeader("pd");
        String url =context.request().path();
        Boolean bypass = context.get(RestBodyHandler.BYPASS_BODY_HANDLER);
        if (Boolean.TRUE.equals(bypass)) {
            // clear flag
            context.put(RestBodyHandler.BYPASS_BODY_HANDLER, Boolean.FALSE);
            context.next();
            return;
        }
        if (isGateWayPath(context, context.request().path())) {
            context.next();
            return;
        }
        EdgeInvocation edgeInvocation = createEdgeInvocation(context);

        edgeInvocation.init(microServiceName, context, url, httpServerFilters);
        logger.info("Matching with header dispatcher,the path {} pattern with {} is router to {} begin,the real path is {}! headeKey is {} headeValue is {}",context.request().path(),url,microServiceName);
        edgeInvocation.edgeInvoke();
        logger.info("Router url {} to {} complete",context.request().path(),microServiceName);
    }


    @Override
    public boolean enabled() {
        return DynamicPropertyFactory.getInstance().getBooleanProperty(KEY_ENABLED, true).get();
    }

}
