package com.je.gateway.router.model.paramValue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * values model定义
 */
public class ConfigValues implements Serializable {
    /**
     * 名称
     */
    private String name;
    /**
     * 值类型
     */
    private String type;
    
    private List<String> values = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void addValue(String value){
        this.values.add(value);
    }
    public void addAllValue(List<String> values){
        this.values.addAll(values);
    }
    public void removeValue(String value){
        this.values.remove(value);
    }

    public List<String> getValues() {
        return values;
    }
}
