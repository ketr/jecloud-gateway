package com.je.gateway.router.loader;

import com.google.common.base.Strings;
import com.je.gateway.router.model.urlRouter.UrlDetail;
import com.je.gateway.router.model.urlRouter.UrlRouter;
import com.je.gateway.xml.AbstractXmlConfigResourceLoader;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

public class UrlRouterResourceLoader extends AbstractXmlConfigResourceLoader<UrlRouter> {

    private static final Logger logger = LoggerFactory.getLogger(UrlRouterResourceLoader.class);

    private static final String DEFAULT_CONFIG_PAHT = "urlRouter.xml";

    /**
     * pattern属性标签
     */
    private static final String ROUTER_PATTERN_ATTRIBUTE_KEY = "pattern";
    /**
     * enable属性标签
     */
    private static final String ROUTER_ENABLE_ATTRIBUTE_KEY = "enable";
    /**
     * suffix path属性
     */
    private static final String ROUTER_PATTERN_URLDETAIL_PATH_KEY = "path";
    /**
     * real path属性
     */
    private static final String ROUTER_PATTERN_URLDETAIL_REALPATH_KEY = "realPath";
    /**
     * suffix microServiceName属性
     */
    private static final String ROUTER_PATTERN_URLDETAIL_MICROSERVICENAME_KEY = "microServiceName";
    /**
     * suffix versionRule属性
     */
    private static final String ROUTER_PATTERN_URLDETAIL_VERSIONRULE_KEY = "versionRule";
    /**
     * suffix prefixSegmentCount属性
     */
    private static final String ROUTER_PATTERN_URLDETAIL_PREFIXSEGEMENTCOUNT_KEY = "prefixSegmentCount";

    public UrlRouterResourceLoader() {
        this(DEFAULT_CONFIG_PAHT);
    }

    public UrlRouterResourceLoader(String resourcePath) {
        super(resourcePath);
    }

    @Override
    public UrlRouter parse(Element rootElement) {
        //获取pattern配置
        String pattern = rootElement.attributeValue(ROUTER_PATTERN_ATTRIBUTE_KEY);
        String enable = rootElement.attributeValue(ROUTER_ENABLE_ATTRIBUTE_KEY);
        UrlRouter urlRouter;
        if(Strings.isNullOrEmpty(pattern)){
            urlRouter = new UrlRouter();
        }else{
            urlRouter = new UrlRouter(pattern);
        }

        if(!Strings.isNullOrEmpty(enable)){
            if("true".equals(enable)){
                urlRouter.setEnable(true);
            }else{
                urlRouter.setEnable(false);
            }
        }

        List<Element> elements = rootElement.elements();
        if(elements == null || elements.isEmpty()){
            return urlRouter;
        }

        for (Element eachElement: elements) {
            urlRouter.addUrl(parseUrlDetail(eachElement));
        }
        return urlRouter;
    }

    private UrlDetail parseUrlDetail(Element urlDetailElement){
        UrlDetail urlDetail = new UrlDetail();
        String path = urlDetailElement.attributeValue(ROUTER_PATTERN_URLDETAIL_PATH_KEY);
        String realPath = urlDetailElement.attributeValue(ROUTER_PATTERN_URLDETAIL_REALPATH_KEY);
        String microServiceName = urlDetailElement.attributeValue(ROUTER_PATTERN_URLDETAIL_MICROSERVICENAME_KEY);
        String versionRule = urlDetailElement.attributeValue(ROUTER_PATTERN_URLDETAIL_VERSIONRULE_KEY);
        String prefixSegementCount = urlDetailElement.attributeValue(ROUTER_PATTERN_URLDETAIL_PREFIXSEGEMENTCOUNT_KEY);
        if(Strings.isNullOrEmpty(path) || Strings.isNullOrEmpty(microServiceName)
                || Strings.isNullOrEmpty(versionRule) || Strings.isNullOrEmpty(prefixSegementCount)){
            logger.error("Parse xml configuration urlRouter.xml error!");
            System.exit(0);
        }
        urlDetail.setPath(path);
        urlDetail.setRealPath(realPath);
        urlDetail.setMicroServiceName(microServiceName);
        urlDetail.setVersionRule(versionRule);
        urlDetail.setPrefixSegmentCount(Integer.valueOf(prefixSegementCount));
        return urlDetail;
    }
}
