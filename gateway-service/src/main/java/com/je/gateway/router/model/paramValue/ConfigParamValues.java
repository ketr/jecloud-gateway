package com.je.gateway.router.model.paramValue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: jecloud-gateway
 * @author: LIULJ
 * @create: 2021/7/4
 * @description:
 */
public class ConfigParamValues implements Serializable {

    private String paramName;

    private String microServiceName;

    private List<String> values = new ArrayList<>();

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getMicroServiceName() {
        return microServiceName;
    }

    public void setMicroServiceName(String microServiceName) {
        this.microServiceName = microServiceName;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public List<String> getValues() {
        return values;
    }

    public void addTable(String value){
        this.values.add(value);
    }

    public void addAll(List<String> values){
        this.values.addAll(values);
    }

    public void removeTable(String value){
        this.values.remove(value);
    }

}
