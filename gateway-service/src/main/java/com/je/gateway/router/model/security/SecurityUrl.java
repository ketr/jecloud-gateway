package com.je.gateway.router.model.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SecurityUrl implements Serializable {

    private String path = "/(.*)";
    private List<String> patterns = new ArrayList<>();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<String> getPatterns() {
        return patterns;
    }

    public void addUrl(String url){
        this.patterns.add(url);
    }

    public void removeUrl(String url){
        this.patterns.remove(url);
    }

}
