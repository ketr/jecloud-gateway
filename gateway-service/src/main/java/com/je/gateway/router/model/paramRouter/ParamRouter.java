package com.je.gateway.router.model.paramRouter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 请求参数路由配置实体
 */
public class ParamRouter implements Serializable {

    private String path;
    private Pattern pattern = Pattern.compile("/(.*)");
    private boolean enable = false;
    private List<ParamUrl> paramUrls = new ArrayList<>();

    public ParamRouter() {
    }

    public ParamRouter(String pattern) {
        this.path = pattern;
        this.pattern = Pattern.compile(pattern);
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
        this.pattern = Pattern.compile(path);
    }

    public Pattern getPattern() {
        return pattern;
    }

    public List<ParamUrl> getParamUrls() {
        return paramUrls;
    }

    public void setParamUrls(List<ParamUrl> paramUrls) {
        this.paramUrls = paramUrls;
    }

    public void addUrl(ParamUrl headerUrl){
        this.paramUrls.add(headerUrl);
    }

    public void removeUrl(ParamUrl headerUrl){
        this.paramUrls.remove(headerUrl);
    }

}
