package com.je.gateway.util;

import io.vertx.ext.web.RoutingContext;
import org.apache.servicecomb.foundation.vertx.http.HttpServletRequestEx;

public class WebUtil {

    public static boolean isMobile(HttpServletRequestEx request) {
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.toLowerCase().indexOf("windows") >= 0) {
            return false;
        } else if (userAgent.toLowerCase().indexOf("mac") >= 0) {
            return false;
        } else if (userAgent.toLowerCase().indexOf("x11") >= 0) {
            return false;
        } else if (userAgent.toLowerCase().indexOf("android") >= 0) {
            return true;
        } else if (userAgent.toLowerCase().indexOf("iphone") >= 0) {
            return true;
        }
        return false;
    }

    public static boolean isMobile(RoutingContext context) {
        String userAgent = context.request().getHeader("User-Agent");
        if (userAgent.toLowerCase().indexOf("windows") >= 0) {
            return false;
        } else if (userAgent.toLowerCase().indexOf("mac") >= 0) {
            return false;
        } else if (userAgent.toLowerCase().indexOf("x11") >= 0) {
            return false;
        } else if (userAgent.toLowerCase().indexOf("android") >= 0) {
            return true;
        } else if (userAgent.toLowerCase().indexOf("iphone") >= 0) {
            return true;
        }
        return false;
    }

}
