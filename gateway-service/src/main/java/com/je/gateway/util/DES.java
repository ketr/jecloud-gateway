package com.je.gateway.util;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

public class DES {

    private SymmetricCrypto des;

    public DES(String key) {
        des = new SymmetricCrypto(SymmetricAlgorithm.DES, key.getBytes());
    }

    /**
     * des解密
     * @param content 密文
     * @return 解密后的明文
     */
    public String decrypt(String content) {
        return des.decryptStr(content);
    }

    /**
     * des加密
     * @param content 明文
     * @return 加密后的密文
     */
    public String encryption(String content) {
        return des.encryptHex(content);
    }

}
