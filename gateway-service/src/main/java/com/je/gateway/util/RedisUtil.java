package com.je.gateway.util;

import org.springframework.data.redis.core.RedisTemplate;

public class RedisUtil {

    private static RedisTemplate<String,Object> redisTemplate;

    static {
        redisTemplate = SpringContextHolder.getBean("redisTemplate");
    }

    public static Object getHash(String key,String subKey){
        return redisTemplate.opsForHash().get(key, subKey);
    }
}
