package com.je.gateway.config;

import org.owasp.esapi.reference.DefaultSecurityConfiguration;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class ESAPIConfiguration extends DefaultSecurityConfiguration {

    @Override
    public InputStream getResourceStream(String filename) throws IOException {
        // 通过类加载器读取resource目录下的配置文件，这个filename就是ESAPI.properties
        String filePath = "";
        if (filename.equals("ESAPI.properties")) {
            filePath = "ESAPI.properties";
        } else if (filename.equals("validation.properties")) {
            filePath = "validation.properties";
        }

        return ClassLoader.getSystemResourceAsStream(filePath);
    }

}