package com.je.gateway.filter;

/**
 * @program: jecloud-gateway
 * @author: LIULJ
 * @create: 2021-07-25 11:36
 * @description:
 */
public class GatewayFilterConstants {
    /**
     * 认证token的Key
     */
    public static final String X_AUTH_TOKEN = "authorization";
    /**
     * 上下文参数字符串
     */
    public static final String CONTEXT_JE_PARAMS_KEY = "jeParams";
}
