package com.je.gateway.filter;

import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.foundation.vertx.http.HttpServletRequestEx;
import org.apache.servicecomb.swagger.invocation.Response;

/**
 * @program: jecloud-gateway
 * @author: LIULJ
 * @create: 2021-07-25 11:39
 * @description: 增加是否是内部RPC调用上下文设置
 */
public class GatewayInternalRequestFilter extends AbstractHttpServerFilter {

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public Response afterReceiveRequest(Invocation invocation, HttpServletRequestEx requestEx) {

        return null;
    }
}
