package com.je.gateway.filter.loader;

import com.je.gateway.xml.AbstractXmlConfigResourceLoader;
import org.dom4j.Element;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-gateway
 * @author: LIULJ
 * @create: 2021/7/8
 * @description:
 */
public class FilterParamsXmlLoader extends AbstractXmlConfigResourceLoader<Map<String,Object>> {

    private static final String DEFAULT_CONFIG_PAHT = "filter.xml";

    public static final String NO_CONVERTION_KEY = "noConversion";

    public FilterParamsXmlLoader() {
        this(DEFAULT_CONFIG_PAHT);
    }

    public FilterParamsXmlLoader(String resourcePath) {
        super(resourcePath);
    }

    @Override
    public Map<String, Object> parse(Element rootElement) {
        Map<String,Object> result = new HashMap<>();
        Element element = rootElement.element(NO_CONVERTION_KEY);
        List<Element> elements = element.elements();
        List<String> noConversionValues = new ArrayList<>();
        for (Element eachElement : elements) {
            noConversionValues.add(eachElement.getStringValue());
        }
        result.put(NO_CONVERTION_KEY,noConversionValues);
        return result;
    }

}
