package com.je.gateway.filter;

import com.google.common.base.Strings;
import org.apache.servicecomb.common.rest.filter.HttpServerFilter;
import org.apache.servicecomb.foundation.vertx.http.HttpServletRequestEx;
import java.net.URLDecoder;
import java.util.Enumeration;

public abstract class AbstractHttpServerFilter implements HttpServerFilter {

    /**
     * 获取字符串参数
     * @param request
     * @param key
     * @return
     */
    public String getStringParameter(HttpServletRequestEx request, String key){
        if(!Strings.isNullOrEmpty(request.getParameter(key))){
            return URLDecoder.decode(request.getParameter(key));
        }else {
            return request.getParameter(key);
        }

        /*if(request.getParameterMap().containsKey(key)){

        }else{
            return request.getAttribute(key) == null ? null: (String) request.getAttribute(key);
        }*/
    }

    /**
     * 获取对象参数
     * @param request
     * @param key
     * @return
     */
    public Object getObjectParamter(HttpServletRequestEx request,String key){
        if(request.getParameterMap().containsKey(key)){
            return request.getParameter(key);
        }else{
            return request.getAttribute(key);
        }
    }

    /**
     * 判断是否包含此参数
     * @param request
     * @param key
     * @return
     */
    public boolean containParameter(HttpServletRequestEx request, String key){
        if(request.getParameterMap().containsKey(key)){
            return true;
        }
        Enumeration<String> enumeration = request.getAttributeNames();
        while(enumeration.hasMoreElements()){
            if(key.equals(enumeration.nextElement())){
                return true;
            }
        }
        return false;
    }
}
