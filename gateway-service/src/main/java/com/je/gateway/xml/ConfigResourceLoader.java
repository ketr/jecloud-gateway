package com.je.gateway.xml;

public interface ConfigResourceLoader<T> {

    T load();

}
