## 3.0.6 (2024-12-24)
### Features
- feat : 修改传参约束大小
- feature : 上传pdf文件时，判断是否存在xss攻击代码

### Bug Fixes
- fix : 开启sql注入，权限加载不出来问题修复

## 3.0.5 (2024-10-30)
### Features
- feature : 添加接口放开，远程打包接口
- feature : 添加接口放开/je/meta/global/file
- feature : 添加接口放开全局脚本
- -feature:更新依赖，解决安全问题
- feature : 添加接口放开/je/meta/global/codes

## 3.0.4 (2024-09-19)
## 3.0.3 (2024-07-26)
### Features
- feat : 修改接口传参大小

### Bug Fixes
- fix : 传递值如果时,时，报错502问题修复

## 3.0.2 (2024-06-25)
### Features
- feat （添加接口放开url）: 全局css样式和图标
- feat （传输加密）: 开启传输加密后，企业微信和钉钉的回调接口不校验加密
- feat （返回数据转换）: 添加自定义数据返回转换。返回的数是大于17位的数字类型时，将数据转成string，否则前端会丢失精度

## 3.0.1 (2024-06-01)
### Features
- feat （拦截器修改）: get请求不修改url信息

### Bug Fixes
- -bugfix:增加通用oauth2单点登录实现
- -bugfix:修复3.0.0抽离base与core后依赖报错的问题

## 3.0.0 (2024-04-25)
### Features
- -feature:调整分发请求顺序，url不能在first pd之前
- -feature:增加证书机制，调整版本至3.0.0
- feat （security）: 添加接口去除权限控制 getWeiXinPermissionsValidationConfig
- feat （security）: 添加接口去除权限控制 /je/document/svc/preview/**
- -feature:增加插件机制

## 2.2.3 (2024-02-23)
### Features
- -feature:抽离nacos
- Merge branch 'develop' into feature/jianguan

## 2.2.2 (2023-12-29)
## 2.2.1 (2023-12-15)
### Features
- feat （rest）: 添加user-agent类型
- feat （oracle）: 添加oracle适配
- feat : 添加rest请求Form大小限制

## 2.2.0 (2023-11-15)
### Features
- Merge branch 'develop' into feature/jianguan

## 2.1.1 (2023-10-27)
### Features
- feature : 添加ESAPI配置文件

## 2.1.0 (2023-10-20)
## 2.0.9 (2023-09-22)
## 2.0.8 (2023-09-15)
### Features
- feature : 修改redis连接配置

## 1.4.1 (2023-09-08)